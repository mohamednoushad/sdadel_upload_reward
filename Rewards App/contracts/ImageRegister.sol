pragma solidity ^0.5.0;


contract Ownable {
    
  address public owner;


  event OwnershipTransferred(address indexed previousOwner, address indexed newOwner);


  
  constructor() public {
      owner = msg.sender;
  }

  /**
   * @dev Throws if called by any account other than the owner.
   */
  modifier onlyOwner() {
    require(msg.sender == owner);
    _;
  }

  function transferOwnership(address newOwner) public onlyOwner {
    require(newOwner != address(0));
    emit OwnershipTransferred(owner, newOwner);
    owner = newOwner;
  }
  
  
}

contract Destructible is Ownable {


  function destroy() onlyOwner() public {
    // selfdestruct(owner);
  }

  function destroyAndSend(address _recipient) onlyOwner public {
    // selfdestruct(_recipient);
  }
}

library SafeMath {

    /**
    * @dev Multiplies two numbers, throws on overflow.
    */
    function mul(uint256 a, uint256 b) internal pure returns (uint256) {
        if (a == 0) {
            return 0;
        }
        uint256 c = a * b;
        assert(c / a == b);
        return c;
    }

    /**
    * @dev Integer division of two numbers, truncating the quotient.
    */
    function div(uint256 a, uint256 b) internal pure returns (uint256) {
        // assert(b > 0); // Solidity automatically throws when dividing by 0
        uint256 c = a / b;
        // assert(a == b * c + a % b); // There is no case in which this doesn't hold
        return c;
    }

    /**
    * @dev Substracts two numbers, throws on overflow (i.e. if subtrahend is greater than minuend).
    */
    function sub(uint256 a, uint256 b) internal pure returns (uint256) {
        assert(b <= a);
        return a - b;
    }


    /**
    * @dev Adds two numbers, throws on overflow.
    */
    function add(uint256 a, uint256 b) internal pure returns (uint256) {
        uint256 c = a + b;
        assert(c >= a);
        return c;
    }
}


contract ImageRegister is Destructible {
    
      event onTokenPurchase(
        address indexed customerAddress,
        uint256 tokensMinted
    );
    

    
    event onWithdraw(
        address indexed customerAddress,
        uint256 ethereumWithdrawn
    );
    
    // ERC20
    event Transfer(
        address indexed from,
        address indexed to,
        uint256 tokens
    );
    
     /*=====================================
    =            CONFIGURABLES            =
    =====================================*/
    string public name = "Neptune";
    string public symbol = "NPT";
    uint8 constant public decimals = 18;
    uint256 public tokensForOneUpload = 1e18;
    
    uint256 public tokenSupply_ = 0;
    
    mapping(address => uint256) public userReceivedTokens;
    
    
    
    struct Image {
        string ipfsHash;        // IPFS hash
        string title;           // Image title
        string description;     // Image description
        string tags;            // Image tags in comma separated format
        uint256 uploadedOn;     // Uploaded timestamp
    }


    mapping (address => Image[]) public ownerToImages;


    bool private stopped = false;


    event LogImageUploaded(
        address indexed _owner, 
        string _ipfsHash, 
        string _title, 
        string _description, 
        string _tags,
        uint256 _uploadedOn
    );


    event LogEmergencyStop(
        address indexed _owner, 
        bool _stop
    );

 
    modifier stopInEmergency { 
        require(!stopped); 
        _;
    }



    function uploadImage(
        string memory _ipfsHash, 
        string memory _title, 
        string memory _description, 
        string memory _tags
    ) public stopInEmergency returns (bool _success) {
            
        require(bytes(_ipfsHash).length == 46);
        require(bytes(_title).length > 0 && bytes(_title).length <= 256);
        require(bytes(_description).length < 1024);
        require(bytes(_tags).length > 0 && bytes(_tags).length <= 256);

        uint256 uploadedOn = now;
        Image memory image = Image(
            _ipfsHash,
            _title,
            _description,
            _tags,
            uploadedOn
        );

        ownerToImages[msg.sender].push(image);

        emit LogImageUploaded(
            msg.sender,
            _ipfsHash,
            _title,
            _description,
            _tags,
            uploadedOn
        );
        
        //tokens 
        userReceivedTokens[msg.sender] += tokensForOneUpload;
        tokenSupply_ = SafeMath.add(tokenSupply_, userReceivedTokens[msg.sender]);
        
        emit onTokenPurchase(msg.sender, tokensForOneUpload);

        _success = true;
    }


    function getImageCount(address _owner) 
        public view 
        stopInEmergency 
        returns (uint256) 
    {
        return ownerToImages[_owner].length;
    }


    function getImage(address _owner, uint8 _index) 
        public stopInEmergency view returns (
        string memory _ipfsHash, 
        string memory _title, 
        string memory _description, 
        string memory _tags,
        uint256 _uploadedOn
    ) {


        require(_index >= 0 && _index <= 2**8 - 1);
        require(ownerToImages[_owner].length > 0);

        Image storage image = ownerToImages[_owner][_index];
        
        return (
            image.ipfsHash, 
            image.title, 
            image.description, 
            image.tags, 
            image.uploadedOn
        );
    }


    function emergencyStop(bool _stop) public onlyOwner {
        stopped = _stop;
        emit LogEmergencyStop(owner, _stop);
    }
    
    
    //token functions
    
    function myTokens() public view returns(uint256) {
        address _customerAddress = msg.sender;
        return balanceOf(_customerAddress);
    }
    
     function balanceOf(address _customerAddress) view public returns(uint256) {
        return userReceivedTokens[_customerAddress];
    }
    
    function transfer(address _to, uint256 _value) public returns (bool success) {
        if (userReceivedTokens[msg.sender] >= _value) {
            userReceivedTokens[msg.sender] -= _value;
            userReceivedTokens[_to] += _value;
            emit Transfer(msg.sender, _to, _value);
            return true;
        } else {
            return false;
        }
    }
    
     function totalSupply()
        public
        view
        returns(uint256)
    {
        return tokenSupply_;
    }
    
    
    
    
}