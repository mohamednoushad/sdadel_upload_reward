import React from 'react'

import './Footer.css'

export default () => {
  return (
    <footer class="footer text-muted">
      <div class="container">
        <p class="float-right">
          <a href="/">Back to top</a>
        </p>
        <p> Copyright &copy; 2020 Neptune Tokens Reward App</p>
      </div>
    </footer>
  )
}
