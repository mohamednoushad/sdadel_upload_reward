import React, { Component } from 'react'
import { Link } from 'react-router-dom'

class Navbar extends Component {
  render() {
    return (
      <header>
        <div className="collapse bg-dark" id="navbarHeader">
          <div className="container">
            <div className="row">
              <div className="col-sm-8 col-md-7 py-4">
                <h4 className="text-white">About</h4>
                <p className="text-white">Rewards App</p>
                <p className="text-muted">
                  This App is intended to reward for the interactions user makes
                  on the platform. You can upload an image and you will be
                  rewarded with 1 Neptune Token. Similarly for the reviews you
                  write on the platform for the image, you will be rewarded with
                  one Neptune Token. Hurry ! Don't miss the chance. Another
                  Interesting Feature is the image that you upload is not stored
                  in a centralised database. Instead it resides in IPFS. IPFS
                  and Blockchain - Welcome to the New Internet.
                </p>
                <div className="copyright text-white mt-3">
                  <p className="mbr-text mbr-fonts-style display-7">
                    © Copyright 2018 Neptune Tokens- All Rights Reserved
                  </p>
                </div>
              </div>
              <div className="col-sm-4 offset-md-1 py-4">
                <h4 className="text-white">For more information</h4>
                <ul className="list-unstyled">
                  <li>
                    <a
                      href="https://en.wikipedia.org/wiki/Ethereum"
                      className="text-white"
                    >
                      Ethereum
                    </a>
                  </li>
                  <li>
                    <a
                      href="https://en.wikipedia.org/wiki/Decentralized_application"
                      className="text-white"
                    >
                      Decentralized application
                    </a>
                  </li>
                  <li>
                    <a
                      href="https://en.wikipedia.org/wiki/Smart_contract"
                      className="text-white"
                    >
                      Smart Contracts
                    </a>
                  </li>
                  <li>
                    <a
                      href="https://en.wikipedia.org/wiki/InterPlanetary_File_System"
                      className="text-white"
                    >
                      InterPlanetary File System
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div className="navbar navbar-dark bg-dark box-shadow">
          <div className="container d-flex justify-content-between">
            <Link to="/" className="navbar-brand d-flex align-items-center">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="20"
                height="20"
                viewBox="0 0 24 24"
                fill="none"
                stroke="currentColor"
                strokeWidth="2"
                strokeLinecap="round"
                strokeLinejoin="round"
                className="mr-2"
              >
                <path d="M23 19a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2V8a2 2 0 0 1 2-2h4l2-3h6l2 3h4a2 2 0 0 1 2 2z" />
                <circle cx="12" cy="13" r="4" />
              </svg>
              <strong>Neptune Rewards App</strong>
            </Link>
            <button
              className="navbar-toggler"
              type="button"
              data-toggle="collapse"
              data-target="#navbarHeader"
              aria-controls="navbarHeader"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <span className="navbar-toggler-icon" />
            </button>
          </div>
        </div>
      </header>
    )
  }
}

export default Navbar
