export const getAccountTokenBalance = () => async (dispatch, getState) => {
  const web3State = getState().web3

  try {
    const tokenBalance = await web3State.contractInstance.balanceOf.call(
      web3State.account,
      {
        from: web3State.account,
      }
    )

    console.log(tokenBalance)

    return tokenBalance
  } catch (error) {
    console.log('error', error)
  }
}
